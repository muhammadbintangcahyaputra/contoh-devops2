stage-build:
  image: docker:18.09-dind
  stage: build
  services:
    - docker:dind
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
  script:
    - . .gitlab-deploy.sh "BUILD"


stage-prod:
  image: ruby:2.5
  stage: deploy
  script:
    - . .gitlab-deploy.sh "DEPLOY"
  # when: manual


stages:
  - build
  - deploy

variables:
  WAKTU: $(date '+%Y-%m-%d.%H')

before_script:
  - echo "$SSH_KEY" > key.pem
  - chmod 400 key.pem

build:
  stage: build
  script:
    - echo '[*] Building Program To Docker Images'
    - echo "[*] Tag $WAKTU"
    - docker build -t strongpapazola/devops-docker:$CI_COMMIT_BRANCH .
    - docker login --username=$DOCKER_USER --password=$DOCKER_PASS
    - docker push strongpapazola/devops-docker:$CI_COMMIT_BRANCH
    - echo $CI_PIPELINE_ID
  only:
    - merge_requests
    - master

deploy:
  stage: deploy
  script:
    - echo "[*] Tag $WAKTU"
    - echo "[*] Deploy to production server in version $CI_COMMIT_BRANCH"
    - echo '[*] Generate SSH Identity'
    - HOSTNAME=`hostname` ssh-keygen -t rsa -C "$HOSTNAME" -f "$HOME/.ssh/id_rsa" -P "" && cat ~/.ssh/id_rsa.pub
    - echo '[*] Execute Remote SSH'
    - ssh -i key.pem -o "StrictHostKeyChecking no" root@104.248.145.189 "docker login --username=$DOCKER_USER --password=$DOCKER_PASS"
    - ssh -i key.pem -o "StrictHostKeyChecking no" root@104.248.145.189 "docker pull strongpapazola/devops-docker:$CI_COMMIT_BRANCH"
    - ssh -i key.pem -o "StrictHostKeyChecking no" root@104.248.145.189 "docker logout"
    - ssh -i key.pem -o "StrictHostKeyChecking no" root@104.248.145.189 "docker stop devops-docker-$CI_COMMIT_BRANCH"
    - ssh -i key.pem -o "StrictHostKeyChecking no" root@104.248.145.189 "docker rm devops-docker-$CI_COMMIT_BRANCH"
    - ssh -i key.pem -o "StrictHostKeyChecking no" root@104.248.145.189 "docker run -d -p 8080:80 --restart always --name devops-docker-$CI_COMMIT_BRANCH strongpapazola/devops-docker:$CI_COMMIT_BRANCH"
    # ssh -i key.pem -o "StrictHostKeyChecking no" root@104.248.145.189 "docker exec farmnode-main sed -i 's/farmnode_staging/farmnode/g' /var/www/html/application/config/database.php"
    - echo $CI_PIPELINE_ID
  only:
    - master